# Created by newuser for 5.8

# PROMPT
[ "$UID" -eq 0 ] && PROMPT="%B%F{blue}[%F{#de0000}%n%F{magenta}:%F{yellow}%~%F{blue}]$%f%b " || PROMPT="%B%F{cyan}[%F{blue}%n%F{magenta}:%F{yellow}%~%F{cyan}]$%f%b "

# autocomplete
autoload -Uz compinit
compinit

zstyle ':completion:*' menu select

# hist
HISTSIZE=5000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# export vi mode
bindkey -v

# alias
# some alias were taken from https://wiki.installgentoo.com/wiki/Bash

alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias bb="newsboat"
alias bbr="newsboat -x reload"
alias diff="diff --color=auto"
alias fhost="ip addr | grep inet; python -m http.server"
alias fucking="sudo"
alias grep='grep --color=auto'
alias fgrep="fgrep --color=auto"
alias egrep="egrep --color=auto"
alias gsreduce="gs -sDEVICE=pdfwrite -dPDFSETTINGS=/$1   -dNOPAUSE -dBATCH -q -o $2 $3"
alias inet="ip a s | grep inet | awk '{ printf \"%s\n\", \$2 }'"
alias ip="ip --color=auto"
alias kk="kjv -W"
alias ls="ls -A --color=auto"
alias l="ls -CF"
alias la="ls -A"
alias ll="ls -lh"
alias lw="ls -l | wc -l"
alias pc="ping gnu.org -c 5"
alias please="sudo"
alias shred="shred -zu"
alias sxit="sxiv -t "
alias tree="tree -a"

# enable hightlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
