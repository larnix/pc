#!/bin/sh

#boomerstuff
set -e

echo "--------------gr33t1ng5 c1t1z3n-----------";

sed -i 's/^#ParallelDownloads = 5&/ParallelDownloads = 15/' /etc/pacman.conf;
timedatectl set-ntp true
lsblk && echo "Enter the drive you want to partition(/dev/sdX): " && read p4rtn4m3 && cfdisk $p4rtn4m3

echo "Creating File Systems..."
#echo "[boot] partition: " && read $bootpart && mkfs.vfat -F 32 $bootpart
echo "[boot] partition: " && read $bootpart && mkfs.ext4 $bootpart
echo "[swap] partition: " && read $swappart && mkswap $swappart
echo "[root] partition: " && read $rootpart && mkfs.ext4 $rootpart

echo "Mounting root partition to /mnt ..." && mount $rootpart /mnt
echo "Mounting boot partition to /mnt/boot ..." && [ ! -d "/mnt/boot" ] && $(mkdir /mnt/boot) &  mount $bootpart /mnt/boot
echo "Turning on Swap ..." && swapon $swappart
echo "Pacstraping the /mnt ..." && pacstrap /mnt base base-devel linux-lts linux-firmware vim
echo "Generating fstab for /mnt ..." && genfstab -U /mnt >> /mnt/etc/fstab
echo "Arch chroot ..." && sed '1,/^#zoomerstuff$/d' pc.sh > /mnt/pc_then.sh && chmod +x /mnt/pc_then.sh&& arch-chroot /mnt/pc_then.sh
exit

#zoomerstuff

sed -i 's/^#ParallelDownloads = 5$/ParallelDownloads = 15/' /etc/pacman.conf
echo "Enter your zoneinfo [Asia/Kolkata]: " && read z0n31nf0 && ln -sf /usr/share/zoneinfo/$z0n31nf0 /etc/localtime
hwclock --systohc
sed "s/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/" /etc/locale.gen
sed "s/^#en_US ISO-8859-1/en_US ISO-8859-1/" /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=us" >> /etc/vconsole.conf
echo "Enter Hostname: " && read h0stn4m3 && echo $h0stn4m3 > /etc/hostname

passwd

pacman --noconfirm --needed -S grub networkmanager > /dev/null 2>&1;
grub-install /boot && grub-mkconfig -o /boot/grub/grub.cfg
systemctl enable NetworkManager.service;

echo "Do you want to setup a user? [y/Y]:" && read y3s2us3r
if [[ "$y3s2us3r" == 'y' || "$y3s2us3r" == 'Y' ]]; then
	echo "%wheel ALL=(ALL) NOPASSWD: ALL\nDefaults !tty_tickets" >> /etc/sudoers
	echo "Setup a user and the passwrord ..."
	echo "Enter username:" && read us3r && useradd -m -G wheel -s /bin/bash $us3r && passwd $us3r
fi

#normiestuff
echo "What about Graphical Server? \
	1: XFCE \
	2: I'll do it on my own" && read gr4s3r
[ "$gr4s3r" == 1 ] &&  pacman -S xfce4 xfce4-goodies && echo "lightdm-session" > ~/.xinitrc && echo "You can Start your Xsession with 'startx' command" && [ "$y3s2us3r" == 'y' || "$y3s2us3r" == 'Y' ] && cp ~/.xinitrc /home/$us3r/ && chown $us3r:wheel /home/$us3r/.xinitrc

[ "$gr4s3r" == 2 ] && echo "There is a script from LARNIX at https://gitlab.om/larnix/pc \nGive it a try by simply running\n\n	curl -sO https://gitlab.com/larnix/pc/raw/master/pacman.sh && chmod +x pacman.sh && ./pacman.sh \n\n"

echo "Finished Setup, reboot now ..."
exit
