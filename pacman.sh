#!/bin/sh

# this script and install dwm and all basic requirements
# curl -sO https://gitlab.com/larnix/pc/raw/master/pacman.sh

set -e # exit this script if there is some error

BLACKARCH="https://blackarch.org/strap.sh"
CYN="\033[0;36m"
END="\033[0m"
LARNIXGIT="https://gitlab.com/larnix"
LARNIXPC="$LARNIXGIT/pc/raw/master"
LARNIXRC="$LARNIXGIT/rc/raw/master"
SOFTWAREURL="$LARNIXPC/software.csv"
MKS="make clean install -s"
BACKGROUNDSDIR="/usr/share/backgrounds"
LOCALBINDIR="/usr/local/bin"
SUCKLESSDIR="/etc/X11/suckless"

installinfo() { printf "\n${CYN} $1 ${END}\n"; }

addpkg() { pacman --noconfirm --needed -S $(cat $1 | awk -F "\"*,\"*" '{ print $1 }') > /dev/null 2>&1; }

clonegit() { git clone $1 $2 -q; }

maclin() { cd $1 && $MKS > /dev/null 2>&1; }

curlstuff() { curl -s $1 -o $2; }

setupBA() { curlstuff $BLACKARCH strap.sh && chmod +x strap.sh && ./strap.sh > /dev/null 2>&1 && shred -zu ./strap.sh; }

echo "[=================================================================================]"
echo ""
echo "                                      LARNIX BASH SCRIPT"
echo "         Hi there! this script will install setup DWM and install stuff for you"
echo "                   so that you can be back up and start doing your work"
echo "                                                                     ~LARNIX"
echo ""
echo "         NOTE:"
echo "              IF THE SCRIPT SCREWED UP ANYTHING DO THIS: "
echo "                   .    REMOVE THE /etc/X11/suckless DIRECTORY"
echo "                   :    CHECK THE INTERNET CONNECTION"
echo "                   :.   RE-RUN THE SCRIPT"
echo "                   ::   DON'T PANIC"
echo "                   ::.  REPORT/PATCH at https://gitlab.com/larnix/pc"
echo "                                                                       "
echo "[=================================================================================]"
echo ""

#read -p "Do you want to install Tools from AUR? [y/n]:" archuserrep
read -p "Do you want to setup BlackArch Repository? [y/Y]:" installsec

# install basic tools
installinfo "[+] Installing Basic Tools" && curl -sO $SOFTWAREURL && addpkg ./software.csv && shred -zu ./software.csv

# install security tools
if [[ "$installsec" == "y" || "$installsec" == "Y" ]]; then
	installinfo "[+] Setting up BlackArch Repository" && setupBA 
fi

# download suckless software
installinfo "[+] Cloning All Git Repositories"
[ -d "$SUCKLESSDIR" ] && rm -rf $SUCKLESSDIR
installinfo "[+] Cloning DWM" && clonegit $LARNIXGIT/dwm.git $SUCKLESSDIR/dwm
installinfo "[+] Cloning Dmenu" && clonegit $LARNIXGIT/dmenu.git $SUCKLESSDIR/dmenu
installinfo "[+] Cloning Slstatus" && clonegit $LARNIXGIT/slstatus.git $SUCKLESSDIR/slstatus
installinfo "[+] Cloning Suckless Terminal" && clonegit $LARNIXGIT/st.git $SUCKLESSDIR/st
installinfo "[+] Cloning Surf" && clonegit https://git.suckless.org/surf $SUCKLESSDIR/surf

# make clean install

installinfo "[+] Installing DWM" && maclin $SUCKLESSDIR/dwm
installinfo "[+] Installing Dmenu" && maclin $SUCKLESSDIR/dmenu
installinfo "[+] Installing SlStatus" && maclin $SUCKLESSDIR/slstatus
installinfo "[+] Installing Suckless Terminal" && maclin $SUCKLESSDIR/st
installinfo "[+] Installing surf browser" && maclin $SUCKLESSDIR/surf

# Setting up dotfiles
installinfo "[+] Setting Up dotfiles"

[ -d "$BACKGROUNDSDIR" ] && rm -rf $BACKGROUNDSDIR 
installinfo "[+] Downloading Wallpapers" && clonegit $LARNIXGIT/backgrounds.git $BACKGROUNDSDIR && rm -rf $BACKGROUNDSDIR/.git
cd $LOCALBINDIR
curlstuff $LARNIXRC/brt ./brt
curlstuff $LARNIXRC/capture ./capture
curlstuff $LARNIXRC/hey ./hey
curlstuff $LARNIXRC/rec ./rec
curlstuff $LARNIXRC/brt ./brt
curlstuff $LARNIXRC/setbg ./setbg
cd ~/
curlstuff $LARNIXPC/.xinitrc ~/.xinitrc
curlstuff $LARNIXPC/.Xresources ~/.Xresources
curlstuff $LARNIXPC/.zshrc ~/.zshrc
curlstuff $LARNIXPC/.zshenv ~/.zshenv

# Setting up ~/.vimrc and ~/.vim_runtime
installinfo "[+] Setting Up ~/.vimrc & ~/.vim_runtime/ \n "
[ -d ~/.vim_runtime ] && rm -rf ~/.vim_runtime;
clonegit $LARNIXGIT/vimrc.git ~/.vim_runtime;
~/.vim_runtime/install_awesome_vimrc.sh > /dev/null 2>&1;
cd ~/;

installinfo "[+] Everything's OK \n"
